#!/bin/bash
git clone https://github.com/shapeshed/express_example.git
cd express_example || exit 1
npm install && \
npm test && \
npm start
